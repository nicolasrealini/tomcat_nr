/*
 * TEnvironment.cpp
 *
 *  Created on: Dec 16, 2018
 *      Author: phaentu
 */

#include "TEnvironment.h"

//-------------------------------------------
// TEnvironment
//-------------------------------------------
TEnvironment::TEnvironment(){
	_numEnv = 0;
	prior_pi = NULL;
	logPriorProposalRatio = 0.0;

	//parameters
	_A = NULL;
	changedIndex_A = 0;
	hasChanged_A = false;
	updatedParameter = NULL;
};

TEnvironment::TEnvironment(int NumEnv){
	_numEnv = NumEnv;
	_initialize(NumEnv);
};

TEnvironment::TEnvironment(std::vector<std::string> & Names){
	_numEnv = 0;
	initialize(Names);
};

TEnvironment::~TEnvironment(){
	clear();
};

void TEnvironment::initialize(std::vector<std::string> & Names){
	_initialize(Names.size());
	envNames = Names;
};

void TEnvironment::_initialize(int NumEnv){
	clear();

	_numEnv = NumEnv;
	_pi.set(0.0);
	_A = new TMCMCParameterMixture[_numEnv];

	set_A_zero();

	//default names
	for(size_t e=0; e<_numEnv; ++e)
		envNames.push_back("Env_" + toString(e+1));

	//restart parameter updating
	hasChanged_A = false;
};

void TEnvironment::clear(){
	if(_numEnv > 0){
		delete[] _A;
		_numEnv = 0;
	}
};

void TEnvironment::set_A_zero(){
	for(size_t e=0; e<_numEnv; ++e)
		_A[e].set(0.0);
};

void TEnvironment::set_A(std::vector<double> & A){
	if(A.size() != _numEnv)
		throw "Can not set A: size of provided vector unequal number of environmental variables!";

	for(size_t e=0; e<_numEnv; ++e)
		_A[e].set(A[e]);
};

void TEnvironment::setPrior_A(TPrior* Prior){
	for(size_t e=0; e<_numEnv; ++e)
		_A[e].addPrior(Prior);
};

void TEnvironment::setJumpToZero_A(double propJumpToZero, TPrior* Prior){
	for(size_t e=0; e<_numEnv; ++e)
		_A[e].setJumpToZero(&_pi, propJumpToZero, Prior);
};

void TEnvironment::simulate(int NumEnv_nonZero, int NumEnv_zero, double sd_A, TRandomGenerator* randomGenerator){
	_initialize(NumEnv_nonZero + NumEnv_zero);

	set_pi((double) NumEnv_nonZero / (double)(NumEnv_nonZero + NumEnv_zero));
	set_A_zero();
	for(int e=0; e<NumEnv_nonZero; ++e)
		_A[e] = randomGenerator->getNormalRandom(0.0, sd_A);
};

void TEnvironment::simulate(int NumEnv_nonZero, int NumEnv_zero, const std::vector<double> & A,  TRandomGenerator* randomGenerator){
	_initialize(NumEnv_nonZero + NumEnv_zero);

	set_pi((double) NumEnv_nonZero / (double)(NumEnv_nonZero + NumEnv_zero));
	set_A_zero();
	for(int e=0; e<NumEnv_nonZero; ++e)
		_A[e] = A[e];
};

void TEnvironment::simulateEnvironmentalVariables(std::vector<double> & vec, TRandomGenerator* randomGenerator){
	vec.clear();
	for(size_t e=0; e<_numEnv; ++e){
		vec.push_back(randomGenerator->getNormalRandom(0.0, 1.0));
	}
};

int TEnvironment::getIndexFromName(const std::string & name){
	for(size_t i=0; i<envNames.size(); ++i){
		if(envNames[i] == name)
			return i;
	}
	return -1;
};

void TEnvironment::addNamesToVector(std::vector<std::string> & vec){
	for(size_t i=0; i<envNames.size(); ++i)
		vec.emplace_back(envNames[i]);
};

std::string TEnvironment::getString_A(){
	std::string ret = toString(_A[0].value);
	for(size_t e=1; e<_numEnv; ++e)
		ret += ", " + toString(_A[e].value);
	return ret;
};

double TEnvironment::getLinearCombination(std::vector<double> & environmentalVariables, double & linearComb){
	linearComb = 0.0;
	for(size_t e=0; e<_numEnv; ++e)
		linearComb += environmentalVariables[e] * _A[e].value;

	return linearComb;
};

double TEnvironment::updateLinearCombination(std::vector<double> & environmentalVariables, double & linearComb){
	if(hasChanged_A){
		linearComb += environmentalVariables[changedIndex_A] * (_A[changedIndex_A].value - _A[changedIndex_A].value_old);
	}

	return linearComb;
};

//-----------------------------------------------------------------
// MCMC updates
//-----------------------------------------------------------------
void TEnvironment::update_pi(TRandomGenerator* randomGenerator){
	//update
	_pi.update(randomGenerator);

	//calc hastings ratio
	double log_h = prior_pi->getLogPriorDensity(_pi.value) - prior_pi->getLogPriorDensity(_pi.value_old);
	int numZero = 0;
	for(size_t e=0; e<_numEnv; ++e){
		if(_A[e].isZero) ++numZero;
	}

	log_h += (_numEnv - numZero) * log(_pi.value / _pi.value_old) + numZero * log((1.0 - _pi.value) / (1.0 - _pi.value_old));

	//accept or reject?
	if(log(randomGenerator->getRand()) > log_h){
		_pi.reject();
	}
};

bool TEnvironment::updateNext_A(TRandomGenerator* randomGenerator){
	//do we restart? Else advance
	if(!hasChanged_A){
		hasChanged_A = true;
		changedIndex_A = 0;
	} else {
		changedIndex_A++;
		if(changedIndex_A == _numEnv){
			hasChanged_A = false;
			return false;
		}
	}

	//set pointer to parameter to update and update
	updatedParameter = &_A[changedIndex_A];
	logPriorProposalRatio = updatedParameter->update(randomGenerator);
	return true;
};

void TEnvironment::rejectLastUpdate_A(){
	updatedParameter->reject();
};

void TEnvironment::adjustProposalRanges(){
	for(size_t e=0; e<_numEnv; ++e)
		_A[e].adjustProposal();

	_pi.adjustProposal();
};

void TEnvironment::writeMCMCHeader(gz::ogzstream & out){
	out << "LL\tpi";
	for(size_t e=0; e<_numEnv; ++e)
		out << "\tA_" << envNames[e];
	out << "\n";
};

void TEnvironment::writeMCMCValues(gz::ogzstream & out, const double LL){
	out << LL << "\t" << _pi.value;
	for(size_t e=0; e<_numEnv; ++e)
		out << "\t" << _A[e].value;
	out << "\n";
};

void TEnvironment::printAcceptanceRates(TLog* logfile, bool estimatePi){
	std::string s = toString(_A[0].getAcceptanceRate());
	for(size_t e=1; e<_numEnv; ++e)
		s += ", " + toString(_A[e].getAcceptanceRate());
	logfile->list("A = " + s);
	if(estimatePi)
		logfile->list("pi = " + toString(_pi.getAcceptanceRate()));
};

//-----------------------------------------------------------------
// TEnvironment_p
//-----------------------------------------------------------------
double TEnvironment_p::getParamValue(std::vector<double> & environmentalVariables, double & linearComb){
	return 1.0 / (1.0 + exp(getLinearCombination(environmentalVariables, linearComb)));
};

double TEnvironment_p::updateParamValue(std::vector<double> & environmentalVariables, double & linearComb){
	return 1.0 / (1.0 + exp(getLinearCombination(environmentalVariables, linearComb)));
};

//-----------------------------------------------------------------
// TEnvironment_lambda
//-----------------------------------------------------------------
double TEnvironment_lambda::getParamValue(std::vector<double> & environmentalVariables, double & linearComb){
	//turn into rate per second: remove log(24*60*60) = 11.36674
	return exp(getLinearCombination(environmentalVariables, linearComb) + _a0.value - 11.36674);
};

double TEnvironment_lambda::updateParamValue(std::vector<double> & environmentalVariables, double & linearComb){
	//turn into rate per second: remove log(24*60*60) = 11.36674
	return exp(getLinearCombination(environmentalVariables, linearComb) + _a0.value - 11.36674);
};

void TEnvironment_lambda::_initialize(int NumEnv){
	TEnvironment::_initialize(NumEnv);
	_a0.set(0.0);
};

void TEnvironment_lambda::simulate(int NumEnv_nonZero, int NumEnv_zero, double a0, double sd_A, TRandomGenerator* randomGenerator){
	TEnvironment::simulate(NumEnv_nonZero, NumEnv_zero, sd_A, randomGenerator);
	set_a0(a0);
};

void TEnvironment_lambda::simulate(int NumEnv_nonZero, int NumEnv_zero, double a0, const std::vector<double> & A, TRandomGenerator* randomGenerator){
	TEnvironment::simulate(NumEnv_nonZero, NumEnv_zero, A, randomGenerator);
	set_a0(a0);
};

void TEnvironment_lambda::adjustProposalRanges(){
	TEnvironment::adjustProposalRanges();
	_a0.adjustProposal();
};

void TEnvironment_lambda::update_a0(TRandomGenerator* randomGenerator){
	logPriorProposalRatio = _a0.update(randomGenerator);
};

void TEnvironment_lambda::reject_a0(){
	_a0.reject();
};

void TEnvironment_lambda::writeMCMCHeader(gz::ogzstream & out){
	out << "LL\ta0\tpi";
	for(size_t e=0; e<_numEnv; ++e)
		out << "\tA_" << envNames[e];
	out << "\n";
};

void TEnvironment_lambda::writeMCMCValues(gz::ogzstream & out, const double LL){
	out << LL << "\t" << _a0.value << "\t" << _pi.value;
	for(size_t e=0; e<_numEnv; ++e)
		out << "\t" << _A[e].value;
	out << "\n";
};

void TEnvironment_lambda::printAcceptanceRates(TLog* logfile, bool estimatePi){
	logfile->list("a0 = " + toString(_a0.getAcceptanceRate()));
	TEnvironment::printAcceptanceRates(logfile, estimatePi);
};


