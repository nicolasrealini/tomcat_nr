/*
 * TSimulator.h
 *
 *  Created on: Dec 15, 2018
 *      Author: phaentu
 */

#ifndef TSIMULATOR_H_
#define TSIMULATOR_H_


#include "TLog.h"
#include "TParameters.h"
#include "TTimeIntervals.h"
#include "TCameraTraps.h"
#include "TEnvironment.h"
#include "TFile.h"

class TSimulator{
private:
	TLog* logfile;
	TTimeIntervals intervals;

	void simulateLocations(const std::string & out, const int & numLocations, TEnvironment_lambda & env_lambda, TRandomGenerator* randomGenerator);

public:
	TSimulator(TLog* Logfile);

	void runSimulations(TParameters & params, TRandomGenerator* randomGenerator);

};




#endif /* TSIMULATOR_H_ */
