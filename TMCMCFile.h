/*
 * TMCMCFile.h
 *
 *  Created on: Mar 25, 2019
 *      Author: phaentu
 */

#ifndef TMCMCFILE_H_
#define TMCMCFILE_H_

#include "stringFunctions.h"
#include "gzstream.h"
#include "TEnvironment.h"

//---------------------------------------
// TMCMCFile
//---------------------------------------
class TMCMCFile{
protected:
	std::string filename;
	gz::igzstream file;
	bool isOpen;
	size_t numCols;
	int lineNum;
	int numSamples;

	void readHeader();

public:
	std::vector<std::string> header;
	std::vector<double> values;

	TMCMCFile();
	TMCMCFile(std::string filename);
	~TMCMCFile();

	void open(std::string filename);
	bool eof(){ return file.eof() || !file.good(); };
	bool readNext();
	int getNumSamplesRead(){ return numSamples; };
};

//---------------------------------------
// TMCMCFile_lambda
//---------------------------------------

class TMCMCFile_lambda:public TMCMCFile{
private:


public:
	TMCMCFile_lambda(){};
	TMCMCFile_lambda(std::string filename);
	void setValues(TEnvironment_lambda & env);
};

#endif /* TMCMCFILE_H_ */
