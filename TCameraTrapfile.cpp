/*
 * TCameraTrapfile.cpp
 *
 *  Created on: Dec 18, 2018
 *      Author: phaentu
 */

#include "TCameraTrapfile.h"

TCameraTrapfile::TCameraTrapfile(){
	fileOpen = false;
	in = NULL;
	numCols = 0;
	lineNum = 0;
	trapStart = 0;
	trapEnd = 0;
};

TCameraTrapfile::TCameraTrapfile(const std::string & filename, std::string tag, TLog* logfile){
	fileOpen = false;
	open(filename, tag, logfile);
};

void TCameraTrapfile::close(){
	if(fileOpen)
		delete in;
};

void TCameraTrapfile::open(const std::string & Filename, std::string tag, TLog* logfile){
	//open file
	filename = Filename;
	close(); //make sure it is closed
	if(filename.find(".gz") == std::string::npos){
		logfile->listFlush(tag + " from file '" + filename + "' ...");
		in = new std::ifstream(filename.c_str());
	} else {
		logfile->listFlush(tag + " from gzipped file '" + filename + "' ...");
		in = new gz::igzstream(filename.c_str());
	}
	fileOpen = true;

	//read header
	lineNum = 1;
	std::getline(*in, line);
	fillVectorFromStringAnySkipEmpty(line, vec, "\t\f\v\n\r");
	if(vec.size() < 4)
			throw "Expecting at least 4 columns: name of camera trap, start date/time, end date/time followed by environmental variables!";

	//save env names and number of columns
	envNames.assign(vec.begin() + 3, vec.end());
	envVariables.resize(envNames.size());

	numCols = vec.size();

	//set variables
	trapStart = 0;
	trapEnd = 0;
};

bool TCameraTrapfile::readNext(){
	if(!fileOpen) throw "No file to read from!";
	if(!in->good() || in->eof()) return false;

	//read line
	++lineNum;
	std::getline(*in, line);

	//split by tabs only
	fillVectorFromStringAnySkipEmpty(line, vec, "\t\f\v\n\r");

	//skip empty lines
	if(vec.size() == 0) return readNext();

	//check if there are three columns: trap date and time
	if(vec.size() != numCols)
		throw "Wrong number of columns in file '" + filename + "' on line " + toString(lineNum) + "! Expected " + toString(numCols) + " but found " + toString(vec.size()) + "!";

	//save camera trap details
	trapName = vec[0];
	trapStart = stringToTime(vec[1]);
	trapEnd = stringToTime(vec[2]);

	//parse environment
	for(int e=0; e<envVariables.size(); ++e)
		envVariables[e] = stringToDouble(vec[3+e]);

	return true;
};

std::string TCameraTrapfile::getLineString(){
	return "in file '" + filename + "' on line " + toString(lineNum);
};


