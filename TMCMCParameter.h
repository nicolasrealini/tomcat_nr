/*
 * TMCMCParameter.h
 *
 *  Created on: Dec 19, 2018
 *      Author: phaentu
 */

#ifndef TMCMCPARAMETER_H_
#define TMCMCPARAMETER_H_

#include "TRandomGenerator.h"

//-------------------------------------------
// TPrior
//-------------------------------------------
class TPrior{
private:

public:
	TPrior(){};
	virtual ~TPrior(){};
	virtual double getLogPriorDensity(double x){ return 0.0; };
	virtual double getRandomValue(TRandomGenerator* randomGenerator){ throw "Can not draw random number from U(-inf, inf) prior!"; };
};

class TPriorUniform:public TPrior{
private:
	double min, max;
	double logDensity;

public:
	TPriorUniform();
	TPriorUniform(double Min, double Max);
	void initialize(double Min, double Max);

	double getLogPriorDensity(double x);
	double getRandomValue(TRandomGenerator* randomGenerator);
};

class TPriorNormal:public TPrior{
private:
	double mean, sd;
	double logOneOverSqrtTwoPiSigma, twoSd;

public:
	TPriorNormal();
	TPriorNormal(double Mean, double Sd);
	void initialize(double Mean, double Sd);
	double getLogPriorDensity(double x);
	double getRandomValue(TRandomGenerator* randomGenerator);
};

class TPriorExponential:public TPrior{
private:
	double lambda;
	double logLambda;

public:
	TPriorExponential();
	TPriorExponential(double Lambda);
	void initialize(double Lambda);
	double getLogPriorDensity(double x);
	double getRandomValue(TRandomGenerator* randomGenerator);
};


//-------------------------------------------
// TMCMCParameter
//-------------------------------------------
class TMCMCParameter{
protected:
	double propSD;
	int updates;
	int acceptedUpdates;
	TPrior* prior;
	bool hasPrior;

public:
	double value;
	double value_old;

	TMCMCParameter();
	TMCMCParameter(double val);
	virtual ~TMCMCParameter(){};

	void addPrior(TPrior* Prior){ prior = Prior; hasPrior = true; };
	void set(double val){ value_old = value; value = val; };
	void scale(double Scale){ value_old = value; value *= Scale; };
	void setPropSD(double SD){ propSD = SD; };
	virtual double update(TRandomGenerator* & randomGenerator); //return log(proposal ratio)
	virtual void reject();
	void reset(){ value = value_old; };
	void adjustProposal();
	virtual double getAcceptanceRate();
};

class TMCMCParameterPositive:public TMCMCParameter{
public:
	TMCMCParameterPositive():TMCMCParameter(){};
	TMCMCParameterPositive(double val):TMCMCParameter(val){};
	~TMCMCParameterPositive(){};

	double update(TRandomGenerator* & randomGenerator);
};

class TMCMCParameterZeroOne:public TMCMCParameter{
public:
	TMCMCParameterZeroOne():TMCMCParameter(){};
	TMCMCParameterZeroOne(double val):TMCMCParameter(val){};
	~TMCMCParameterZeroOne(){};

	double update(TRandomGenerator* & randomGenerator);
	void adjustProposal();
};

class TMCMCParameterMixture:public TMCMCParameter{
private:
	bool isZero_old;
	int numWasZero;
	double probJumpToZero; //rate at which a move to zero is proposed
	double logProbJumpToZero;
	TPrior* priorJumpAwayFromZero; //distribution from which to pick a value when moving away from zero
	TMCMCParameter* pi; //hierarchical probability that A != 0

public:
	bool isZero;

	TMCMCParameterMixture();
	TMCMCParameterMixture(double val);
	~TMCMCParameterMixture(){};

	void setJumpToZero(TMCMCParameter* Pi, double probabilityToJump, TPrior* Prior);
	void set(double val);
	double update(TRandomGenerator* & randomGenerator);
	void reject();
};



#endif /* TMCMCPARAMETER_H_ */
