/*
 * TEnvVar.h
 *
 *  Created on: Jan 7, 2019
 *      Author: phaentu
 */

#ifndef TENVVAR_H_
#define TENVVAR_H_

#include "TEnvironment.h"
#include "TMeanVar.h"

//--------------------------------------------
// TEnvVar
//--------------------------------------------
class TEnvVar{
public:
	std::vector<double> variables;
	bool isStandardized;

	double value;
	double value_old;
	double linearComb;
	double linearComb_old;

	TEnvVar(){
		value = 0.0;
		value_old = 0.0;
		linearComb = 0.0;
		linearComb_old = 0.0;
		isStandardized = false;
	};

	TEnvVar(int size){
		value = 0.0;
		value_old = 0.0;
		init(size);
	};

	~TEnvVar(){};

	void init(int size){
		variables.clear();
		variables.resize(size);
	};

	void init(std::vector<double> & Env, TEnvironment* env){
		variables.assign(Env.begin(), Env.end());
		value = env->getParamValue(variables, linearComb);
	};

	void init(std::vector<double> & Env, TEnvironment* env, TMeanVar* mv){
		variables.assign(Env.begin(), Env.end());
		standardize(mv);
		value = env->getParamValue(variables, linearComb);
	};

	double getMostExtremeValue(){
		//return the most extreme value in units of SD
		double max = 0.0;
		for(size_t i=0; i<variables.size(); ++i){
			if(fabs(variables[i]) > max)
				max = fabs(variables[i]);
		}
		return max;
	};

	int size(){
		return variables.size();
	};

	void update(TEnvironment* env){
		value_old = value;
		linearComb_old = linearComb;
		value = env->updateParamValue(variables, linearComb);
	};

	void reset(){
		value = value_old;
		linearComb = linearComb_old;
	};

	void write(gz::ogzstream & out){
		for(std::vector<double>::iterator it=variables.begin(); it!=variables.end(); it++)
			out << "\t" << *it;
	};

	void addToMeanVar(TMeanVar* mv){
		for(size_t i=0; i<variables.size(); ++i)
			mv[i].add(variables[i]);
	};

	void standardize(TMeanVar* mv){
		if(isStandardized)
			throw "Env variables has already been standardized!";
		for(size_t i=0; i<variables.size(); ++i)
			variables[i] = (variables[i] - mv[i].mean()) / mv[i].sd();
		isStandardized = true;
	};
};




#endif /* TENVVAR_H_ */
