/*
 * TMCMCFile.cpp
 *
 *  Created on: Mar 25, 2019
 *      Author: phaentu
 */

#include "TMCMCFile.h"

//---------------------------------------
// TMCMCFile
//---------------------------------------
TMCMCFile::TMCMCFile(){
	isOpen = false;
	lineNum = 0;
	numSamples = 0;
	numCols = 0;
};

TMCMCFile::TMCMCFile(std::string Filename){
	open(Filename);
};

TMCMCFile::~TMCMCFile(){
	if(isOpen)
		file.close();
};

void TMCMCFile::open(std::string Filename){
	filename = Filename;
	if(filename.empty()) throw "Please provide a valid filename!";
	file.open(filename.c_str());
	if(!file)
		throw "Failed to open file '" + filename + "' for reading!";

	isOpen = true;
	lineNum = 0;
	numSamples = 0;

	//read header and first values
	readHeader();
};

void TMCMCFile::readHeader(){
	std::string line;
	getline(file, line);
	fillVectorFromStringWhiteSpaceSkipEmpty(line, header);
	numCols = header.size();
	++lineNum;
};

bool TMCMCFile::readNext(){
	if(!isOpen)
		throw "MCMC file was never opend!";
	if(file.eof() || !file.good())
		return false;

	std::string line;
	getline(file, line);
	fillVectorFromStringWhiteSpaceSkipEmpty(line, values);
	++lineNum;

	//skip empty lines
	if(values.size() == 0) return readNext();

	//check if there are three columns: env, mean, sd
	if(values.size() != numCols)
		throw("Wrong number of columns in file '" + filename + "' on line " + toString(lineNum) + "! Expected " + toString(numCols) + " but found " + toString(values.size()) + ".");

	++numSamples;
	return true;
};

//---------------------------------------
// TMCMCFile_lambda
//---------------------------------------
TMCMCFile_lambda::TMCMCFile_lambda(std::string filename):TMCMCFile(filename){

};

void TMCMCFile_lambda::setValues(TEnvironment_lambda & env){
	//update environment
	std::vector<double> envVals;
	envVals.assign(values.begin() + 3, values.end());
	env.set_A(envVals);
	env.set_a0(values[1]);
	env.set_pi(values[2]);
};

