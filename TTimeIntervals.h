/*
 * TTimeIntervals.h
 *
 *  Created on: Dec 15, 2018
 *      Author: phaentu
 */

#ifndef TTIMEINTERVALS_H_
#define TTIMEINTERVALS_H_

#include "stringFunctions.h"
#include "TParameters.h"
#include "TRandomGenerator.h"
#include <vector>
#include <math.h>
#include "gzstream.h"
#include "TMCMCParameter.h"

//----------------------------------------
//TTimeIntervals
//----------------------------------------
class TTimeIntervals{
private:
	int numActivityIntervals_nh;
	int numObservationPerActivityInterval;
	int numObservationIntervals_n;
	int activityLength; //in seconds
	int observationlength; //in seconds
	int numSecondsPerDay; //86400 (24*60*60)
	time_t referenceDate; //used to simply calculate intervals through divisions

	TMCMCParameterPositive* K;
	double* K_scaled; //scaled by observation interval length
	int shift, shift_old;
	int numShiftUpdates, numShiftUpdatesAccepted;
	short* observationToActivityMap;
	bool initialized;
	int curK; //used during MCMC to keep track of updating
	double logPriorProposalRatio;

	void normalizeK();
	void fillObservationToActivityMap(int Shift);
	void fillKScaled();

public:
	TTimeIntervals();
	TTimeIntervals(int NumActivityIntervals, int NumObservationIntervals, int Shift);
	TTimeIntervals(TParameters & params, TLog* logfile);
	TTimeIntervals(std::vector<std::string> & mcmcFileHeader);
	~TTimeIntervals();

	void initialize(int NumActivityIntervals, int NumObservationIntervals, int Shift);
	void initialize(std::vector<std::string> & mcmcFileHeader);
	void setReferenceDate(time_t time);
	void setK(std::vector<double> K_vec);
	void setKUniform();
	void setFromMcmcLine(std::vector<double> & mcmcLine);
	double calcLogLikelihood(const double & lambdaBar, const double & p, int*** data_sums);

	//get functions
	double getK(const double timeOfDayFraction);
	int getNumActivityIntervals(){ return numActivityIntervals_nh; };
	int getNumObservationIntervals(){ return numObservationIntervals_n; };
	int getActivityIntervalLength(){ return activityLength; };
	int getObservationIntervalLength(){ return observationlength; };
	double getKObservationInterval(int interval){ return K[observationToActivityMap[interval]].value; };
	int getNumSecondsPerDay(){ return numSecondsPerDay; };
	int getObservationInterval(time_t time);
	int getNextObservationInterval(time_t time);
	time_t getStartOfObservationInterval(time_t day, int interval);
	std::string getKString();
	int getNumObservationsPerActivityInterval() { return numObservationPerActivityInterval; };

	bool updateNextK(TRandomGenerator* randomGenerator);
	double getLogPriorProposalRatioLastUpdate(){ return logPriorProposalRatio; };
	void rejectLastUpdate_K();
	void updateShift(TRandomGenerator* randomGenerator);
	void rejectShitUpdate();
	void adjustProposalRanges();
	void writeMCMCHeader(gz::ogzstream & out);
	void writeMCMCValues(gz::ogzstream & out, const double LL);
	void printAcceptanceRates(TLog* logfile);
};


#endif /* TTIMEINTERVALS_H_ */
