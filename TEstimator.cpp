/*
 * TEstimator.cpp
 *
 *  Created on: Dec 16, 2018
 *      Author: phaentu
 */

#include "TEstimator.h"

TEstimator::TEstimator(TLog* Logfile, TRandomGenerator* RandomGenerator){
	logfile = Logfile;
	randomGenerator = RandomGenerator;
	trapsInitialized = false;
	traps = NULL;
	intervalsInitialized = false;
	intervals = NULL;
	estimatePi = false;
};

TEstimator::~TEstimator(){
	if(intervalsInitialized)
		delete intervals;
	if(trapsInitialized)
		delete traps;
	for(std::vector<TPrior*>::iterator it = priors.begin(); it != priors.end(); ++it)
		delete *it;
};

void TEstimator::initializeIntervals(TParameters & params){
	intervals = new TTimeIntervals(params, logfile);
	intervalsInitialized = true;
};

void TEstimator::initializeTraps(){
	traps = new TCameraTraps(&env_lambda, &env_p, intervals);
	trapsInitialized = true;
};

void TEstimator::runEstimation(TParameters & params){
	//initialize intervals
	initializeIntervals(params);

	//read traps
	logfile->startIndent("Reading data:");
	initializeTraps();
	trapFileName_lambda = params.getParameterString("traps_lambda");
	if(trapFileName_lambda == "") throw "Please provide a valid filename with traps_lambda.";
	trapFileName_p = params.getParameterString("traps_p");
	if(trapFileName_p == "") throw "Please provide a valid filename with traps_p.";
	traps->readTraps(trapFileName_lambda, trapFileName_p, logfile);

	//standardize environmental variables
	bool standardize = params.getParameterIntWithDefault("standardize", 1);
	if(standardize){
		//get names of files to write mean and variance to
		std::string meanSDFile_lambda = trapFileName_lambda;
		meanSDFile_lambda = extractBefore(meanSDFile_lambda, '.');
		meanSDFile_lambda += "_MeanSD.txt.gz";
		std::string meanSDFile_p = trapFileName_p;
		meanSDFile_p = extractBefore(meanSDFile_p, '.');
		meanSDFile_p += "_MeanSD.txt.gz";

		logfile->listFlush("Normalizing environmental variables ...");
		traps->normalizeEnvVariables(meanSDFile_lambda, meanSDFile_p, logfile);
		logfile->done();
		logfile->conclude("Wrote mean and SD for lambda to file '" + meanSDFile_lambda + "'.");
		logfile->conclude("Wrote mean and SD for p to file '" + meanSDFile_p + "'.");
	}

	//read data
	observationFileName = params.getParameterString("observations");
	traps->readObservations(observationFileName, logfile);
	logfile->endIndent();

	//read priors
	initializePriors(params);

	//estimate initial parameters
	guessInitialParameters();

	//run MCMC
	runMCMC(params);
};

void TEstimator::initializePriors(TParameters & params){
	//read priors
	logfile->startIndent("Reading Priors:");
	//a0
	double priorSD_a0 = params.getParameterDoubleWithDefault("priorSD_a0", 0.1);
	logfile->list("Will use a normal prior on a0 with SD = " + toString(priorSD_a0) + ".");
	priors.emplace_back(new TPriorNormal(0.0, priorSD_a0));
	env_lambda.setPrior_a0(*priors.rbegin());

	//A_lambda
	double priorSD_lambda = params.getParameterDoubleWithDefault("priorSD_A_lambda", 0.1);
	logfile->list("Will use a normal prior on A for lambda with SD = " + toString(priorSD_lambda) + ".");
	priors.emplace_back(new TPriorNormal(0.0, priorSD_lambda));
	env_lambda.setPrior_A(*priors.rbegin());

	//A_p
	double priorSD_p = params.getParameterDoubleWithDefault("priorSD_A_p", 0.1);
	logfile->list("Will use a normal prior on A for p with SD = " + toString(priorSD_p) + ".");
	priors.emplace_back(new TPriorNormal(0.0, priorSD_p));
	env_p.setPrior_A(*priors.rbegin());

	//proposal of mixture models
	logfile->startIndent("Will update A's as follows:");
	double propToZero = params.getParameterDoubleWithDefault("jumpToZero", 0.1);
	logfile->list("Will propose a move A -> 0 with probability " + toString(propToZero) + ".");
	double lambdaAwayFromZero = params.getParameterDoubleWithDefault("propLambda_A", 1.0);
	logfile->list("When moving from A=0 to A=x, will choose x~Exp(" + toString(lambdaAwayFromZero) + ").");
	priors.emplace_back(new TPriorExponential(lambdaAwayFromZero));
	env_lambda.setJumpToZero_A(propToZero, *priors.rbegin());
	env_p.setJumpToZero_A(propToZero, *priors.rbegin());

	//pi
	//check if we estimate
	if(params.parameterExists("priorLambda_pi")){
		double priorLambda_pi = params.getParameterDoubleWithDefault("priorLambda_pi", 100);
		logfile->list("Will use an exponential prior on pi with lambda = " + toString(priorLambda_pi) + ".");
		priors.emplace_back(new TPriorExponential(priorLambda_pi));
		env_lambda.setPrior_pi(*priors.rbegin());
		env_p.setPrior_pi(*priors.rbegin());
		estimatePi = true;
	} else {
		//pi lambda
		double pi_lambda = params.getParameterDoubleWithDefault("pi_lambda", 1.0 / (double) env_lambda.size());
		if(pi_lambda <= 0.0 || pi_lambda > 1.0)
			throw "pi_lambda must be within range (0.0, 1.0]!";
		env_lambda.set_pi(pi_lambda);
		logfile->list("Fixing pi_lambda to " + toString(pi_lambda) + ".");

		//pi_p
		double pi_p = params.getParameterDoubleWithDefault("pi_p", 1.0 / (double) env_p.size());
		if(pi_p <= 0.0 || pi_p > 1.0)
			throw "pi_lambda must be within range (0.0, 1.0]!";
		env_p.set_pi(pi_p);
		logfile->list("Fixing pi_p to " + toString(pi_p) + ".");

		estimatePi = false;
	}

	logfile->endIndent();
};

void TEstimator::guessInitialParameters(){
	logfile->startIndent("Guessing initial parameters:");
	//set all K = 1
	intervals->setKUniform();
	logfile->list("K = " + intervals->getKString() + ".");

	//guess average lambda from fraction of intervals with data assuming p=0.5
	//Note: lambdaBar has units 1/day
	double fracWithObs = traps->getFractionOfIntervalsWithObservations();
	double lambdaBar = -2.0 * log(1.0 - fracWithObs) / intervals->getObservationIntervalLength() * intervals->getNumSecondsPerDay();
	double a0_lambda = log(lambdaBar);

	env_lambda.set_a0(a0_lambda);
	logfile->list("Estimated lambdaBar = " + toString(lambdaBar) + ", implying a0_lambda = " + toString(a0_lambda) + ".");

	//guess A_p, then A_lambda
	guessInitialA(&env_p, "Guessing initial A_p");
	guessInitialA(&env_lambda, "Guessing initial A_lambda");

	//now update traps
	traps->update();
	logfile->list("Initial LL = " + toString(traps->getLL()) + ".");

	logfile->endIndent();
};

void TEstimator::guessInitialA(TEnvironment* env, std::string progressString){
	std::vector<double> tmpA(env->size(), 0.0);
	std::vector<double> A(env->size(), 0.0);

	//iteratively find highest abs(A) given all previously found
	int numAToFind = env->size() > 10 ? 10 : env->size();
	std::vector<bool> Aset(env->size(), false);

	for(int i=0; i<numAToFind; i++){
		logfile->listOverFlush(progressString + " ... (" + toString(round(100 * (double) i / (double) numAToFind)) + "%)");

		//fill vector with estimates of A for those not yet set
		for(size_t e=0; e<env->size(); e++){
			if(!Aset[e]){
				tmpA.assign(A.begin(), A.end());
				estimateAPeakFinder(env, tmpA, e);
			}
		}

		//find highest abs(A) not yet set
		double max = 0.0;
		int maxIndex = 0;
		for(size_t e=0; e<env->size(); e++){
			if(!Aset[e] && fabs(tmpA[e]) > max){
				max = fabs(tmpA[e]);
				maxIndex = e;
			}
		}

		//set highest A
		A[maxIndex] = tmpA[maxIndex];
		Aset[maxIndex] = true;

		//check if hihest A is super small
		if(A[maxIndex] < 0.001) break;
	}

	logfile->overList(progressString + " ... done!   ");

	std::string out = "Set A!=0 for these environmental variables: ";
	bool first = true;
	for(size_t e=0; e<env->size(); e++){
		if(Aset[e]){
			if(first) first = false;
			else out += ", ";
			out += env->getName(e);
		}
	}
	logfile->conclude(out);

	//now set estimated A
	env->set_A(A);
};

double TEstimator::estimateAPeakFinder(TEnvironment* env, std::vector<double> & A, int index){
	A[index] = 0.0;
	env->set_A(A);
	double LL = traps->update();

	//run stupid 1D search to find peak
	double step = 0.1;
	for(int s=0; s<50; s++){
		A[index] += step;
		env->set_A(A);
		double newLL = traps->update();
		if(newLL < LL) //switch if LL decreases
			step = - step / 2.718282;

		LL = newLL;
	}

	return A[index];
};

void TEstimator::runMCMC(TParameters & params){
	logfile->startIndent("Running MCMC inference:");

	//reading MCMC parameters
	//iterations
	logfile->startIndent("Reading MCMC parameters:");
	int iterations = params.getParameterIntWithDefault("iterations", 10000);
	logfile->list("Will run an MCMC for " + toString(iterations) + " iterations.");

	//burnin
	int burnin  = params.getParameterIntWithDefault("burnin", 1000);
	int numBurnin = params.getParameterIntWithDefault("numBurnin", 2);
	if(numBurnin > 0)
		logfile->list("Will run " + toString(numBurnin) + " burnins of " + toString(burnin) + " iterations each.");

	//output names
	std::string out = params.getParameterString("out", false);
	if(out == ""){
		//construct output from observation file name
		out = observationFileName;
		out = extractBefore(out, '.');
		out += "_";
	}
	logfile->list("Will write output files with prefix '" + out + "'.");

	//thinning
	int thinning = params.getParameterIntWithDefault("thinning", 1);
	if(thinning < 1) throw "Thinning must be > 0!";
	if(thinning == 1)
		logfile->list("Will write full chain.");
	else if(thinning == 2)
		logfile->list("Will write every second iteration.");
	else if(thinning == 3)
		logfile->list("Will write every third iteration.");
	else
		logfile->list("Will write every " + toString(thinning) + "th iteration.");
	logfile->endIndent();

	//open MCMC file
	openMCMCOutputFiles(out);

	//run burnin
	if(numBurnin > 0){
		if(numBurnin > 1)
			logfile->startNumbering("Running " + toString(numBurnin) + " burnins:");
		else
			logfile->startNumbering("Running " + toString(numBurnin) + " burnin:");
		for(int b=0; b<numBurnin; ++b){
			logfile->number("Burnin number " + toString(b+1) + ":");
			logfile->addIndent(2);
			runBurnin(burnin);
			logfile->removeIndent(2);
		}
		logfile->endIndent();
	}

	//run MCMC chain
	logfile->startIndent("Running MCMC chain:");
	runMCMC(iterations, thinning);
	logfile->endIndent();

	//close files
	mcmcOut_K.close();
	mcmcOut_lambda.close();
	mcmcOut_p.close();
};

void TEstimator::runBurnin(int len){
	std::string report = "Running a burnin of " + toString(len) + " iterations ... ";
	logfile->listFlush(report + "(0%)");

	int prog = 0;
	int prog_old = 0;
	for(int i=0; i<len; ++i){
		runMCMCIteration();

		//report progress
		prog = 100 * (double) i / (double) len;
		if(prog > prog_old){
			logfile->listOverFlush(report + "(" + toString(prog) + "%)");
			prog_old = prog;
		}
	}
	logfile->overList(report + "done! ");

	//report acceptance rates
	reportAcceptanceRates();

	//adjust proposal ranges
	logfile->listFlush("Adjusting proposal parameters ...");
	env_lambda.adjustProposalRanges();
	env_p.adjustProposalRanges();
	intervals->adjustProposalRanges();
	logfile->done();
};

void TEstimator::runMCMC(int len, int thinning){
	std::string report = "Running an MCMC chain of " + toString(len) + " iterations ... ";
	logfile->listFlush(report + "(0%)");

	int prog = 0;
	int prog_old = 0;
	for(int i=0; i<len; ++i){
		runMCMCIteration();

		//print to file?
		if(i % thinning == 0)
			writeCurrentParameters();

		//report progress
		prog = 100 * (double) i / (double) len;
		if(prog > prog_old){
			logfile->listOverFlush(report + "(" + toString(prog) + "%)");
			prog_old = prog;
		}
	}
	logfile->overList(report + "done! ");

	//report acceptance rates
	reportAcceptanceRates();
};

void TEstimator::runMCMCIteration(){
	//update all environmental parameters for lambda bar
	while(env_lambda.updateNext_A(randomGenerator)){
		if(!traps->updateLambdaBar(randomGenerator))
			env_lambda.rejectLastUpdate_A();
	}

	//update all environmental parameters for p
	while(env_p.updateNext_A(randomGenerator)){
		if(!traps->updateP(randomGenerator))
			env_p.rejectLastUpdate_A();
	}

	//update hierarchical parameters (a0 and pi)
	env_lambda.update_a0(randomGenerator);
	if(!traps->updateLambdaBar(randomGenerator))
		env_lambda.reject_a0();

	if(estimatePi){
		env_lambda.update_pi(randomGenerator);
		env_p.update_pi(randomGenerator);
	}

	//update intervals K and shift
	while(intervals->updateNextK(randomGenerator)){
		if(!traps->updateK(randomGenerator))
			intervals->rejectLastUpdate_K();
	}

	intervals->updateShift(randomGenerator);
	if(!traps->updateK(randomGenerator))
		intervals->rejectShitUpdate();
};

void TEstimator::reportAcceptanceRates(){
	logfile->startIndent("Acceptance rates lambda:");
	env_lambda.printAcceptanceRates(logfile, estimatePi);
	logfile->endIndent();

	logfile->startIndent("Acceptance rates p:");
	env_p.printAcceptanceRates(logfile, estimatePi);
	logfile->endIndent();

	logfile->startIndent("Acceptance rates K:");
	intervals->printAcceptanceRates(logfile);
	logfile->endIndent();
};

void TEstimator::openMCMCOutputFile(gz::ogzstream & file, std::string filename){
	file.open(filename.c_str());
		if(!file) throw "Failed to open file '" + filename + "' for writing";
};

void TEstimator::openMCMCOutputFiles(std::string & out){
	//open file for K
	openMCMCOutputFile(mcmcOut_K, out + "MCMC_K.txt.gz");
	intervals->writeMCMCHeader(mcmcOut_K);

	//open file for lambda
	openMCMCOutputFile(mcmcOut_lambda, out + "MCMC_lambda.txt.gz");
	env_lambda.writeMCMCHeader(mcmcOut_lambda);

	//open file for p
	openMCMCOutputFile(mcmcOut_p, out + "MCMC_p.txt.gz");
	env_p.writeMCMCHeader(mcmcOut_p);
};

void TEstimator::writeCurrentParameters(){
	intervals->writeMCMCValues(mcmcOut_K, traps->getLL());
	env_lambda.writeMCMCValues(mcmcOut_lambda, traps->getLL());
	env_p.writeMCMCValues(mcmcOut_p, traps->getLL());
};
