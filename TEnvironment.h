/*
 * TEnvironment.h
 *
 *  Created on: Dec 16, 2018
 *      Author: phaentu
 */

#ifndef TENVIRONMENT_H_
#define TENVIRONMENT_H_

#include "stringFunctions.h"
#include "TRandomGenerator.h"
#include "gzstream.h"
#include "TLog.h"
#include "TMCMCParameter.h"

//-------------------------------------------
// TEnvironment
//-------------------------------------------
class TEnvironment{
protected:
	size_t _numEnv;
	std::vector<std::string> envNames;

	//parameters
	TMCMCParameterZeroOne _pi;
	TMCMCParameterMixture* _A;

	//prior and updates
	TPrior* prior_pi;
	double logPriorProposalRatio;

	//variables to update parameters in turns
	bool hasChanged_A;
	size_t changedIndex_A;
	TMCMCParameter* updatedParameter;


	virtual void _initialize(int NumEnv);

public:
	TEnvironment();
	TEnvironment(int NumEnv);
	TEnvironment(std::vector<std::string> & Names);
	virtual ~TEnvironment();

	void initialize(std::vector<std::string> & Names);
	void clear();
	void set_pi(double pi){ _pi = pi; };
	double get_pi(){ return _pi.value; };
	void set_A_zero();
	void set_A(std::vector<double> & A);
	void setPrior_pi(TPrior* Prior){ prior_pi = Prior; };
	void setPrior_A(TPrior* Prior);
	void setJumpToZero_A(double propJumpToZero, TPrior* Prior);
	void simulate(int NumEnv_nonZero, int NumEnv_zero, double sd_A, TRandomGenerator* randomGenerator);
	void simulate(int NumEnv_nonZero, int NumEnv_zero, const std::vector<double> & A,  TRandomGenerator* randomGenerator);
	void simulateEnvironmentalVariables(std::vector<double> & vec, TRandomGenerator* randomGenerator);

	size_t size(){ return _numEnv; };

	int getIndexFromName(const std::string & name);
	std::string getName(int env){ return envNames[env]; };
	std::string getNames(std::string delim){ return getConcatenatedString(envNames, delim); };
	void addNamesToVector(std::vector<std::string> & vec);
	double getLogPriorProposalRatioLastUpdate(){ return logPriorProposalRatio; };
	std::string getString_A();

	double getLinearCombination(std::vector<double> & environmentalVariables, double & linearComb);
	double updateLinearCombination(std::vector<double> & environmentalVariables, double & linearComb);
	virtual double getParamValue(std::vector<double> & environmentalVariables, double & linearComb){ return getLinearCombination(environmentalVariables, linearComb); };
	virtual double updateParamValue(std::vector<double> & environmentalVariables, double & linearComb){ return getLinearCombination(environmentalVariables, linearComb); };

	void update_pi(TRandomGenerator* randomGenerator);
	bool updateNext_A(TRandomGenerator* randomGenerator);
	void rejectLastUpdate_A();
	virtual void adjustProposalRanges();

	virtual void writeMCMCHeader(gz::ogzstream & out);
	virtual void writeMCMCValues(gz::ogzstream & out, const double LL);
	virtual void printAcceptanceRates(TLog* logfile, bool estimatePi);
};

class TEnvironment_lambda:public TEnvironment{
protected:
	TMCMCParameter _a0;
	TPrior* prior_a0;
	void _initialize(int NumEnv);

public:
	TEnvironment_lambda():TEnvironment(){ prior_a0 = NULL; };
	TEnvironment_lambda(int numEnv):TEnvironment(numEnv){ prior_a0 = NULL; };
	TEnvironment_lambda(std::vector<std::string> & Names):TEnvironment(Names){ prior_a0 = NULL; };

	void set_a0(double a0){ _a0 = a0; };
	void setPrior_a0(TPrior* Prior){ prior_a0 = Prior; };
	void simulate(int NumEnv_nonZero, int NumEnv_zero, double a0, double sd_A, TRandomGenerator* randomGenerator);
	void simulate(int NumEnv_nonZero, int NumEnv_zero, double a0, const std::vector<double> & A, TRandomGenerator* randomGenerator);
	double getParamValue(std::vector<double> & environmentalVariables, double & linearComb);
	double updateParamValue(std::vector<double> & environmentalVariables, double & linearComb);
	void adjustProposalRanges();
	void update_a0(TRandomGenerator* randomGenerator);
	void reject_a0();
	void writeMCMCHeader(gz::ogzstream & out);
	void writeMCMCValues(gz::ogzstream & out, const double LL);
	void printAcceptanceRates(TLog* logfile, bool estimatePi);
};

class TEnvironment_p:public TEnvironment{
public:
	TEnvironment_p():TEnvironment(){};
	TEnvironment_p(int numEnv):TEnvironment(numEnv){};
	TEnvironment_p(std::vector<std::string> & Names):TEnvironment(Names){};
	double getParamValue(std::vector<double> & environmentalVariables, double & linearComb);
	double updateParamValue(std::vector<double> & environmentalVariables, double & linearComb);
};

#endif /* TENVIRONMENT_H_ */
