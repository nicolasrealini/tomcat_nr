/*
 * TProjector.h
 *
 *  Created on: Jan 7, 2019
 *      Author: phaentu
 */

#ifndef TPREDICTOR_H_
#define TPREDICTOR_H_

#include "TParameters.h"
#include "TEnvVar.h"
#include <algorithm>
#include "TFile.h"
#include "TMCMCFile.h"
#include "TDeltaTEstimator.h"

//---------------------------------------
// TLocation
//---------------------------------------
class TLocation{
private:
	std::string coor1, coor2;
	TEnvVar envValues;

	std::vector<double> densities;

public:
	TLocation(std::string Coor1, std::string Coor2, std::vector<double> & EnvValues, TMeanVar* mv, TEnvironment* Env);

	double getMostExtremeValue(){ return envValues.getMostExtremeValue(); };
	double getDensity(TEnvironment* Env);
	void addDensity(TEnvironment* Env);
	void writePosteriorDensities(TOutputFile* out);
};

//---------------------------------------
// TPredictorEnvironment
//---------------------------------------
class TPredictorEnvironment{
private:
	TEnvironment_lambda env_lambda;
	TMCMCFile_lambda mcmcFile_lambda;
	TMeanVar* mv;
	bool mvInitialized;
	int* envIndex;
	bool envIndexInitialized;
	std::vector<TLocation> locations;

	void initEnvironment(std::string & filename);
	void readStandardization(std::string & filename);


public:
	TPredictorEnvironment();
	TPredictorEnvironment(std::string mcmcFileName, std::string stdFileName, TLog* logfile);
	~TPredictorEnvironment();

	void initialize(std::string mcmcFileName, std::string stdFileName, TLog* logfile);
	void matchEnvironmentToLocations(std::vector<std::string> & locationHeader);
	void addLocation(std::vector<std::string> & locationLine);
	int getNumLocationsWithExtremValues();
	void predictFromMCMCFile(TLog* logfile, int limitLines);
	void writePosteriorDensities(const std::string & coor1Name, const std::string & coor2Name, std::string & filename);
	bool fillVectorOfNormalizedDensitiesForNextMCMCSample(std::vector<double> & densities);
};

//---------------------------------------
// TPredictor_base
//---------------------------------------
class TPredictor_base{
protected:
	TLog* logfile;
	std::string locationsFileName;
	std::string coor1Name, coor2Name;

	TPredictorEnvironment env;

	void readLocations(TParameters & params);
	virtual void addLocationsToEnvironments(std::vector<std::string> & vec);
	virtual void matchEnvironmentToLocations(std::vector<std::string> & vec);


public:
	TPredictor_base(TLog* logfile);
	virtual ~TPredictor_base(){};
};

//---------------------------------------
// TPredictor
//---------------------------------------
class TPredictor:public TPredictor_base{
public:
	TPredictor(TLog* Logfile):TPredictor_base(Logfile){};
	~TPredictor(){};

	void predict(TParameters & params);
	void estimateDeltaE(TParameters & params);
};

//---------------------------------------
// TDeltaSEstimator
//---------------------------------------
class TDeltaSEstimator:public TPredictor_base{
private:
	TPredictorEnvironment env2;
	std::vector<double> densities1;
	std::vector<double> densities2;
	TMeanVar meanVar;

	std::string openEnvironments(TParameters & params);
	void addLocationsToEnvironments(std::vector<std::string> & vec);
	void matchEnvironmentToLocations(std::vector<std::string> & vec);
	bool readNext();
	double calculateCurrentDeltaS();
	double calculateCurrentDeltaST(TDeltaTEstimator & deltaTEstimator, TMeanVar & thisMeanVar);
	void printMeanVar();

public:
	TDeltaSEstimator(TLog* Logfile):TPredictor_base(Logfile){};
	~TDeltaSEstimator(){}

	void estimateDeltaSpace(TParameters & params);
	void estimateDeltaSpaceTime(TParameters & params);
};


#endif /* TPREDICTOR_H_ */
